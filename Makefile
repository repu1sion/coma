# remember to place *.c files before libs!!!

CC = gcc
#HEADERS = -I/usr/include/SDL
HEADERS =
#LIBS = -L/usr/lib/x86_64-linux-gnu/ -lSDL -lSDL_gfx -lSDL_ttf -lSDL_image
LIBS =
TARGET = coma

all:
	$(CC) $(HEADERS) main.c $(LIBS) -o $(TARGET)

install:
	mkdir -p /mnt/lfs/usr/share/coma
	cp coma comapack /mnt/lfs/usr/bin/
	cp *.sh /mnt/lfs/usr/share/coma

clean:
	rm $(TARGET)
#	rm *.o 

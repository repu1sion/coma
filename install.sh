#!/bin/bash

#need to pass path to the pkg.coma as param
#script stored in /usr/share/coma

TEMPDIR="/tmp/coma/unpack"

#check param was passed
if [ ! $# -eq 1 ]; then
        echo "please enter path to the package!"
        exit 1
fi

# clear previous temp directory if there is so
if [ -d $TEMPDIR ]; then
	rm -rf $TEMPDIR
fi

# creating temp directory
mkdir -p $TEMPDIR
if [ ! $? -eq "0" ]; then
	echo "failed to create temp directory. exiting"
	exit 1
fi

#extracting
cp $1 $TEMPDIR
cd $TEMPDIR
tar xf $1
if [ ! $? -eq "0" ]; then
	echo "failed to unpack. exiting"
	exit 1
fi

if [ -f metacoma ]; then
	PKGNAME=$(cat metacoma | cut -f1 -d" ")
else
	echo "no meta info about package found. exiting"
	exit 1
fi

cd pkg/
#installation
cp -r * /
if [ ! $? -eq "0" ]; then
	echo "failed to install. exiting"
	exit 1
fi
#generate filelist
find . -name "*" > filelist
#remove unneeded dot at the beginning
sed -i 's/^.//g' filelist
#reverse filelist
mv filelist filelist_orig
tac filelist_orig > filelist
#create dir for our package and copy filelist to it
mkdir /usr/share/coma/pkgs/${PKGNAME}
cp filelist /usr/share/coma/pkgs/${PKGNAME}
if [ ! $? -eq "0" ]; then
	echo "failed to generate filelist. exiting"
	exit 1
else
	echo "package $PKGNAME installed successfully"
	cp ../metacoma /usr/share/coma/pkgs/${PKGNAME}
	exit 0
fi

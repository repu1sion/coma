#include <stdio.h>
//below 3 are for dir creation
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
//for exit()
#include <stdlib.h>
//for strlen()
#include <string.h>

#define VERSION "0.13"

#define PROGRAM_DIR "/usr/share/coma"
#define PACKAGES_DIR "/usr/share/coma/pkgs/"
#define DB_NAME "/usr/share/coma/comadb"

//name limits
#define MAX_DB_LINE 512
#define MAX_PACKAGE_NAME 32
#define MAX_VERSION_NAME 16

//max possible deps per package (firefox has like 31 - half of them are optional)
#define MAX_DEPS 32


#define ERROR_DBG
#define DEBUG

#ifdef ERROR_DBG
 #define error(x...) printf("<error> " x)
#else
 #define error(x...)
#endif

#ifdef DEBUG
 #define debug(x...) printf(x)
#else
 #define debug(x...)
#endif


// ----- structs ---------------------------------------------------------------

//general struct
typedef struct coma_s
{
	FILE *db;
	unsigned int packages;

} coma_t;

coma_t coma = {
	.db = NULL,
	.packages = 0,
};

//package format
//1088 bytes now: 32 name + 16 ver + 32*32 deps + 2*8 ptrs
//if we would have 1000 packages it will be like 1Mb in RAM which is allowable
typedef struct package_s
{
	char name[MAX_PACKAGE_NAME];
	char version[MAX_VERSION_NAME];
	char deps[MAX_DEPS][MAX_PACKAGE_NAME];
	struct package_s *next, *prev;
} package_t;

// ----- prototypes ------------------------------------------------------------
package_t* read_metadata(char *path);
int remove_db_line(char *name);

// ----- package lists API------------------------------------------------------
/*
--- lists ---
pkg_add()	- add new pkg to list
pkg_show()	- show all pkgs
pkg_find()	- find pkg in list by name
pkg_del()	- delete single packet
pkg_del_all()	- delete all packages

--- create/remove ---
pkg_create()
pkg_install()
pkg_uninstall()
*/

package_t *head = NULL, *tail = NULL;
                
void pkg_add(package_t *pkg)
{
        if (!head)      //first element added
        {                                                                                                                              
                //debug("head created\n");                                                                                             
                head = pkg;                                                                                                            
                tail = pkg;
                pkg->prev = tail;                                                                                                      
                pkg->next = head;                                                                                                      
        }
        else                                                                                                                           
        {
                //debug("added to tail\n");                                                                                            
                pkg->prev = tail;                                                                                                      
                pkg->next = head;
                tail->next = pkg; //set to previous element pointer to this element                                                    
                tail = pkg;                                                                                                            
                head->prev = tail; //head should always know previous element to loop back                                             
        }
        debug("package [%s] added to db\n", pkg->name);                                                                             
}               
                
void pkg_show()
{
        int cnt = 0;
        package_t* now = head;                                                                                                         
        if (!now)                                                                                                                      
        {                                                                                                                              
                debug("%s() database is empty!\n", __func__);                                                                                    
                return;
        }                                                                                                                              
        
        debug("-----list of packages:----- \n");                                                                                       
                        
        while(1)                                                                                                                       
        {                                                                                                                              
                debug("#%d. %s \n", ++cnt, now->name);                                                                                   
                now = now->next; 
                if (now == head) 
                        break;
        }                                                                                                                              

	debug("total packages installed: %d \n", cnt);
}

//find package from list by given name
package_t* pkg_find(char *pkg_name)
{
        package_t* now = head;

        if (!now)
        {
                debug("%s() database is empty!\n", __func__);                                                                                    
                return NULL;
        }
        while(1)
        {
                if (!strcmp(pkg_name, now->name)) //found it!
                        return now;
                now = now->next;
                if (now == head)
                {
                        debug("package [%s] not found\n", pkg_name);
                        return NULL;
                }
        }
}

void pkg_del(package_t *pkg)
{
	package_t *prev = pkg->prev, *next = pkg->next;
	if (pkg == head && pkg == tail) //last element
	{
		head = NULL;
		tail = NULL;
	}
	else
	{
		prev->next = next;
		next->prev = prev;
		if (pkg == head && pkg != tail)
			head = next;		//we declare next element to be a new head
		else if (pkg == tail && pkg != head)
			tail = prev;		//we declare prev element to be a new tail
	}

	free(pkg);
}

//called from quit() to free memory
void pkg_del_all()
{

        package_t *now = head, *next;
        if (!now)
        {
                error("%s() database is empty!\n", __func__);                                                                                    
                return;
        }

        while(1)
        {
                next = now->next;
                //debug("profile [%s] was erased\n", now->name);
                free(now);
                now = next;
                if (now == head)
		{
			head = NULL;
			tail = NULL;
                        break;
		}
        }
}

//create internal struct for a package
//return ptr if ok, NULL if fail
package_t* pkg_create()
{
        //debug("%s() \n", __func__);

        package_t *pkg = malloc(sizeof(package_t));
        if (!pkg)
        {
                error("can't alloc memory for package\n");
                return NULL;
        }
        else
        {
                coma.packages++; //increase total number of created profiles 
                debug("total packages: %u \n", coma.packages);
        }

        return pkg;
}


//1. if pkgname contains .cma - install from given path			
//2. if no such .cma in current dir - look for network server - XXX
//3. check - is package in DB or not
//4. if not - install in DB - pkg_create(), pkg_add(newpkg), add_db_line(newpkg)
int pkg_install(char *pkgname)
{
	int rv = 0;
	int n;
	int namechars = 0;
	char name[MAX_PACKAGE_NAME] = {0};
	char execstring[256] = {0};
	char pathstring[256] = {0};
	char *p;
	
	int local_install = 0; 	//if .cma found - switch to 1
	struct stat st = {0};

	debug("trying to install package: %s\n", pkgname);
	
	//#1. Search - is pkgname ended with ".cma"
	n = strlen(pkgname);
	//debug("n: %d \n", n);
	if (n > 4)
	{
		p = pkgname + n - 4;
		//debug("pkgname: %p , p: %p \n", pkgname, p);
		debug ("pkg name ended with: %s \n", p);
		if (!strncmp(".cma", p, 4))
		{
			local_install = 1;
			debug("found .cma so install local pkg\n");
		}
		else
		{
			debug("no local package found\n");
		}
	}

	if (local_install)
	{
		//check if path is correct and file exists
		if (stat(pkgname, &st) == -1)
		{
			error("package %s doesn't exist\n", pkgname);
			rv = 1; return rv;
		}
		else
		{
			//extract name from given path (we still have p pointed to .)
			while (--p >= pkgname)
			{
				//debug("%c ", *p);
				if (*p == '/')
					break;
				namechars++;
			}
			strncpy(name, ++p, namechars);	//shift p from '/' to start of name
			//debug("name of package extracted from path is: %s \nnamechars: %d \n", name, namechars);

			//check do we have pkg with such name in DB already or not
			if (pkg_find(name))
			{
				debug("we already have package with name %s installed! \n", name);
				rv = 2; return rv;
			}
			else
			{
				//installing package, executing install script with path to the package as param
				strcat(execstring, "/usr/share/coma/install.sh ");
				strcat(execstring, pkgname); //we add path to the package to pass it as param
				rv = system(execstring);
				if (rv)
				{
					error("error %d during installing package. aborting. \n", rv);
					return rv;
				}

				strcpy(pathstring, PACKAGES_DIR);
				strcat(pathstring, name);
				strcat(pathstring, "/");
				strcat(pathstring, "metacoma");
				if (read_metadata(pathstring) == NULL) //this func will create a package and add it
				{
					error("error %d during metadata read. aborting.\n", rv);
					rv = 3; return rv;
				}
			}
		}
	}
	else
	{
		//looking for server - XXX
	
	}

	return rv;
}

int pkg_uninstall(char *pkgname)
{
	int rv;
	package_t *pkg;
	char execstring[256] = {0};
	char cmpname[MAX_PACKAGE_NAME+1] = {0}; //need to add space to pkgname

	//check do we have pkg with such name in DB already or not
	pkg = pkg_find(pkgname);
	if (!pkg)
	{
		debug("we don't have package with name %s installed! \n", pkgname);
		rv = 1; return rv;
	}
	else
	{
		//uninstalling package, executing uninstall script with package name as param
		strcat(execstring, "/usr/share/coma/uninstall.sh ");
		strcat(execstring, pkgname);
		rv = system(execstring);
		if (rv)
		{
			error("error %d while uninstalling package. aborting. \n", rv);
			return rv;
		}

		//delete package from linked list in RAM
		pkg_del(pkg);

		//delete db line
		strcpy(cmpname, pkgname);
		strcat(cmpname, " ");
		rv = remove_db_line(cmpname);
		if (rv)
		{
			error("failed to remove package from DB\n");
			return rv;
		}
	}

	debug("package uninstalled successfully\n");

	return rv;
}

// ----- functions -------------------------------------------------------------

// ----- processing args--------------------------------------------------------
void show_help()
{
	printf("Usage:\n\t -i - install\n"
		"\t -u - uninstall\n"
		"\t -l - list installed packages\n"
		"\t -h - this help\n");
}

//wrong or no args
void show_error()
{
	error("please add an argument. if not sure - call with -h\n");

}

//just a wrapper to show packages
void show_packages()
{
	pkg_show();
}

// ----- DB API-----------------------------------------------------------------
//read one line of db and create package if its here, then add package to list
package_t* read_db_line(char *line_p)
{
	int i;
	package_t *rv = NULL;
	int first_pos = 0, last_pos = 0, space_cnt = 0;
	package_t pkg, *pkg_p;
	pkg_p = &pkg;
	memset(&pkg, 0x0, sizeof(package_t));
	
	for (i = 0; i < strlen(line_p)+1; i++)
	{
		if (line_p[i] == 0x20 || line_p[i] == 0xA) //space or newline
		{
			if (space_cnt == 0)
			{
				strncpy(pkg_p->name, line_p+first_pos, last_pos-first_pos);
			}
			else if (space_cnt == 1)
			{
				strncpy(pkg_p->version, line_p+first_pos, last_pos-first_pos);
			}
			else
			{
				strncpy(pkg_p->deps[space_cnt-2], line_p+first_pos, last_pos-first_pos);
			}
			space_cnt++;
			last_pos++;		//yes, we increase it twice if we found a space
			first_pos = last_pos;
			//debug("item found\n");
		}
		else
			last_pos++;
	}

	//allocate new pkg and add it to linked list
	if (space_cnt > 1) //we have pkg name and version at least
	{
		package_t *newpkg = pkg_create();
		memcpy(newpkg, &pkg, sizeof(package_t));
		pkg_add(newpkg);
		rv = newpkg;
	}
	else
	{
		debug("%s() didn't find valid info to create and add a package\n", __func__);
		rv = NULL;
	}

	return rv;

}

//add line with new package to DB
int add_db_line(package_t *pkg)
{
	int i;
	int rv = 0;
        char line[MAX_DB_LINE] = {0};
	char str[2] = {0};

	strcpy(line, pkg->name);
	str[0] = 0x20; //space
        strcat(line, str);
        strcat(line, pkg->version);

	for (i = 0; i < MAX_DEPS; i++)
	{
		if (pkg->deps[i])
		{
        		strcat(line, pkg->deps[i]);
        		strcat(line, " ");
		}
		else	//no more deps found
			break;
	}
	str[0] = 0xA; //newline
        strcat(line, str);
	fputs(line, coma.db);
	fflush(coma.db);
	return rv;
}

int remove_db_line(char *name)
{
	int rv = 1;
	int n;
	FILE *f2;	//new file for DB
        char line[MAX_DB_LINE] = {0};

	n = strlen(name);
	f2 = fopen("/tmp/comadb","w+");
	if (!f2) {printf("<error : can't create/open tmpfile. exiting>"); exit(1);}

	//reopen db to reread it again
	fclose(coma.db);
	coma.db = fopen(DB_NAME, "r+");
	if (!coma.db)
	{
		printf("<error : can't create/open db. exiting>");
		exit(1);
	}

	while(fgets(line, sizeof(line), coma.db))
	{
		//we compare "name+space" for every line. copy in new file if not equal
		if (strncmp(name, line, n))
		{
			fputs(line, f2);
		}
		else
		{
			rv = 0;
			debug("package %s found and removed in DB\n", name);
		}	
	}

	fclose(coma.db);
	fclose(f2);
	remove(DB_NAME); 
	rename("/tmp/comadb", DB_NAME);

	//reopen coma.db again
	coma.db = fopen(DB_NAME, "r+");
	if (!coma.db)
	{
		printf("<error : can't create/open db. exiting>");
		exit(1);
	}

	return rv;
}

//read metacoma file from given path. also creates pkg (inside of read_db_line()) and adds to db
package_t* read_metadata(char *path)
{
	FILE *f;
        char line[MAX_DB_LINE] = {0};
	package_t *newpkg = NULL;

	f = fopen(path, "r");
	if (!f)
	{
		error("can't open metadata file: %s \n", path);
		fclose(f);
		return NULL;
	}
	debug("opened metadata file : %s successfully \n", path);

	fgets(line, sizeof(line), f);
	debug("line from metadata: %s", line);
	newpkg = read_db_line(line);
	if (!newpkg)
	{
		error("can't find valid metadata\n");
		fclose(f);
		return NULL;
	}
	else
	{
		add_db_line(newpkg);
	}

	fclose(f);

	return newpkg;
}

//needs to create db if we run it for a first time
int init()
{
	int rv = 0;
	struct stat st = {0};
        char line[MAX_DB_LINE] = {0};

	//create dirs
	if (stat(PROGRAM_DIR, &st) == -1)
		rv = mkdir(PROGRAM_DIR, 0700);
	if (rv)
	{
		printf("<error : can't create program dir. exiting>");
		exit(1);
	}

	if (stat(PACKAGES_DIR, &st) == -1)
		rv = mkdir(PACKAGES_DIR, 0700);
	if (rv)
	{
		printf("<error : can't create program dir. exiting>");
		exit(1);
	}

	//create db
	if (stat(DB_NAME, &st) == -1)
		coma.db = fopen(DB_NAME, "w+");
	else
		coma.db = fopen(DB_NAME, "r+");

	if (!coma.db)
	{
		printf("<error : can't create/open db. exiting>");
		exit(1);
	}

	//read db from file into ram
	while(fgets(line, sizeof(line), coma.db))
	{
		read_db_line(line);
	}


	return rv;
}

int main(int argc, char *argv[])
{
	int rv;

	printf("coma started. version %s \n", VERSION);

	rv = init();

	if (argc != 2 && argc != 3)
	{
		show_error();
		goto end;
	}
	else
	{
		if (!strcmp(argv[1], "-h") || !strcmp(argv[1], "h"))
			show_help();
		else if (!strcmp(argv[1], "-l") || !strcmp(argv[1], "l"))
			show_packages();
		else if (!strcmp(argv[1], "-i") || !strcmp(argv[1], "i"))
			(argc == 3)?pkg_install(argv[2]):show_error();
		else if (!strcmp(argv[1], "-u") || !strcmp(argv[1], "u"))
			(argc == 3)?pkg_uninstall(argv[2]):show_error();
		else
			show_error();

	}



end:
	fclose(coma.db);

	return rv;
}

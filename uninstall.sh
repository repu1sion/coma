#!/bin/bash

#need to pass name as param
#script stored in /usr/share/coma

#check param was passed
if [ ! $# -eq 1 ]; then
        echo "please enter package name to remove!"
        exit 1
fi

#check package is installed
cd /usr/share/coma/pkgs/$1
if [ ! $? -eq "0" ]; then
	echo "failed to find package $1 . exiting"
	exit 1
fi

# read filelist of given package and removing files/dirs one by one
k=1
while read line; do
        #echo "Line # $k: $line"
	if [ -f $line ]; then
		rm -f $line &> /dev/null
	elif [ -d $line ]; then
		rmdir $line &> /dev/null
	fi
        ((k++))
done < filelist

rm -rf /usr/share/coma/pkgs/$1
if [ ! $? -eq "0" ]; then
	echo "failed to remove package $1 . exiting"
	exit 1
fi

echo "package $1 uninstalled"
#echo "Total number of lines in file: $k"

exit 0




